
    
The mie submodule
==================

This submodule provides a set of functions to simulate radial profiles via Mie simulation. All angles has to be interpreted as degrees.

The simulations depends on two different kinds of quantities. 
The lengths, like the radius of the sample, must be provided in unit of the wavelength. For example, to simulate the Mie scattering of a sphere with radius R=120 nm with a radiation wavelength :math:`\lambda` of 30nm, the input radius :math:`R_i` provided to the function must be:

.. math::

    R_i = \frac{R}{\lambda} = 4
    

Mie simulations also depend on the optical properties of the sample material, defined by the complex refractive index :math:`n=n' + \mathbf{i} n''`.
However, it is often more convenient, and well spread in literature, to identify the optical properties via two quantities:


    - :math:`\delta`: refractive index decrease
    - :math:`\beta`: absorpion coefficient
        
        
Their relationship with the complex refractive index :math:`n` is defined by the following relations:

.. math::
    :nowrap:
    
    \begin{eqnarray*}    
    n' &=& 1 - \delta \\
    n''&=& \beta\\
    n &=& 1 - \delta + \mathbf{i} \beta
    \end{eqnarray*}
    
    
List of functions
------------------

.. automodule:: nux_utils.mie
    :members:
    :imported-members:
